import { Module } from '@nestjs/common';
import { ApiController } from 'api/core/controllers/api.controller';
import { ApiService } from 'api/core/services/api.service';

@Module({
  imports: [],
  controllers: [ApiController],
  providers: [ApiService],
})
export class AppModule {}
